from app import app, db

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db.create_all()


class TestBooks():

    def test_books_succeeds(self):

        client = app.test_client()

        response = client.get('/books')

        assert 200 == response.status_code

    def test_books_is_list(self):

        client = app.test_client()

        response = client.get('/books')

        assert isinstance(response.json, list)

    def test_books_can_be_appended(self):

        client = app.test_client()
        book = {"owner": "test_owner", "name": "test_name"}

        client.post('/books', json=book)
        response = client.get('/books')

        assert book in response.json

    def test_spells_in_book_succeeds(self):

        client = app.test_client()
        book = {"owner": "test_owner", "name": "test_name"}

        response = client.get(f'/books/{book["owner"]}/{book["name"]}')

        assert 200 == response.status_code

    def test_spells_in_book_is_list(self):

        client = app.test_client()
        book = {"owner": "test_owner", "name": "test_name"}

        response = client.get(f'/books/{book["owner"]}/{book["name"]}')

        assert isinstance(response.json, list)


class TestCharacters():

    def test_characters_succeeds(self):

        client = app.test_client()

        response = client.get('/characters')

        assert 200 == response.status_code

    def test_characters_is_list(self):

        client = app.test_client()

        response = client.get('/characters')

        assert isinstance(response.json, list)

    def test_characters_can_be_appended(self):

        client = app.test_client()
        character = {"name": "test_owner"}

        client.post('/characters', json=character)
        response = client.get('/characters')

        assert character in response.json

    def test_characters_have_books_list(self):

        client = app.test_client()
        character = {"name": "test_character"}

        response = client.get(f'/books/{character["name"]}')

        assert isinstance(response.json, list)

    def test_character_owns_at_least_one_book(self):

        client = app.test_client()
        character = {"name": "test_owner"}
        book = {"owner": "test_owner", "name": "test_name"}

        response = client.get(f'/books/{character["name"]}')

        assert book in response.json


class TestSpells():

    def test_spells_succeed(self):

        client = app.test_client()

        response = client.get('/spells')

        assert 200 == response.status_code

    def test_spells_is_list(self):

        client = app.test_client()

        response = client.get('/spells')

        assert isinstance(response.json, list)

    def test_spells_can_be_appended_full_spell(self):

        client = app.test_client()
        spell = {
            "name": "Test spell",
            "level": 1,
            "school": "Test School",
            "cast_time": "1 Action",
            "duration": "1 Round",
            "damage_type": "Fire",
            "concentration": False,
            "ritual": False,
            "upcast": False,
            "materials_consumed": False,
            "materials_cost": 0.00
            }

        client.post('/spells', json=spell)
        response = client.get('/spells')

        assert spell in response.json

    def test_spells_can_be_appended_partial_spell(self):

        client = app.test_client()
        spell = {
            "name": "Test spell 1",
            "school": "Test School",
            "cast_time": "1 Action"
            }

        client.post('/spells', json=spell)
        response = client.get('/spells')

        spellinresponse = False
        for returnedSpell in response.json:
            if returnedSpell["name"] == spell["name"]:
                spellinresponse = True

        assert spellinresponse

    def test_spells_in_book_can_be_appended(self):

        client = app.test_client()
        book = {"owner": "test_owner", "name": "test_name"}
        spell = {
            "name": "Test spell"
            }

        client.post(f'/books/{book["owner"]}/{book["name"]}', json=spell)
        response = client.get(f'/books/{book["owner"]}/{book["name"]}')
        spellinresponse = False

        for returnedSpell in response.json:
            if returnedSpell["name"] == spell["name"]:
                spellinresponse = True

        assert spellinresponse
