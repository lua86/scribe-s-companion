import {DataToHeaders} from "./helpers.js"
describe('DataToHeaders', () => {
    const DATA_EMPTY = {}
    const DATA_ONE = {name: "Fancy Name"}
    const HEADERS_ONE = ["Name"]
    const DATA_BOOK = {"name":"Awakened Spellbook","owner":"Bird"}
    const HEADERS_BOOK = ["Name", "Owner"]
    const DATA_MATERIALS = {"materials_consumed":false,"materials_cost":null}
    const HEADERS_MATERIALS = ["Materials consumed","Materials cost"]
    it('Returns an array', () => {
	let headers = DataToHeaders(DATA_EMPTY)
	expect(headers.constructor).toBe([].constructor)
    })
    it('Returns an empty array from empty data', () => {
	let headers = DataToHeaders(DATA_EMPTY)
	expect(headers).toBe([])
    })
    it('Returns an array with one string for data with one property', () => {
	let headers = DataToHeaders(DATA_ONE)
	expect(headers.length).toBe(HEADERS_ONE.length)
    })
    it('Returns capitalised properties', () => {
	let headers = DataToHeaders(DATA_BOOK)
	expect(headers).toBe(HEADERS_BOOK)
    })
    it('Turns underscores into spaces', () => {
	let headers = DataToHeaders(DATA_MATERIALS)
	expect(headers).toBe(HEADERS_MATERIALS)
    })
})
