export function DataToHeaders(data) {
    let headers = []
    headers = Object.keys(data).map( (key) => {
	key = capitalise(key)
	key = underscoreToSpace(key)
	return key
    })
    return headers
}
function capitalise(word) {
    word = word.replace(/^\w/, (initial) => {
	return initial.toUpperCase()
    })
    return word
}
function underscoreToSpace(word) {
    word = word.replace(/_/, () => {
	return " "
    })
    return word
}
