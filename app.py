from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from dataclasses import dataclass


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////app/dev.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['JSON_SORT_KEYS'] = False
db = SQLAlchemy(app)


@app.route('/books', methods=['GET'])
def list_books():

    return jsonify(Book.query.all())


@app.route('/books', methods=['POST'])
def add_books():

    name = request.json.get("name")
    owner = request.json.get("owner")
    book = Book(name=name, owner=owner)

    db.session.add(book)
    db.session.commit()

    return "OK"


@app.route('/characters', methods=['GET'])
def list_characters():
    return jsonify(Character.query.all())


@app.route('/characters', methods=['POST'])
def add_characters():

    name = request.json.get("name")
    character = Character(name=name)

    db.session.add(character)
    db.session.commit()

    return "OK"


@app.route('/books/<character>', methods=['GET'])
def books_from_character(character):

    return jsonify(Book.query.filter_by(owner=character).all())


@app.route('/spells', methods=['GET'])
def all_spells():

    return jsonify(Spell.query.all())


@app.route('/spells', methods=['POST'])
def add_spells():

    name = request.json.get("name")
    level = request.json.get("level")
    school = request.json.get("school")
    cast_time = request.json.get("cast_time")
    duration = request.json.get("duration")
    damage_type = request.json.get("damage_type")
    concentration = request.json.get("concentration")
    ritual = request.json.get("ritual")
    upcast = request.json.get("upcast")
    materials_consumed = request.json.get("materials_consumed")
    materials_cost = request.json.get("materials_cost")

    spell = Spell(name=name,
                  level=level, school=school, cast_time=cast_time,
                  duration=duration, damage_type=damage_type,
                  concentration=concentration, ritual=ritual, upcast=upcast,
                  materials_consumed=materials_consumed,
                  materials_cost=materials_cost)

    db.session.add(spell)
    db.session.commit()

    return "OK"


@app.route('/books/<owner>/<name>', methods=['GET'])
def get_spells_in_book(owner, name):
    return jsonify(Book.query.filter_by(owner=owner, name=name).first().spells)


@app.route('/books/<owner>/<name>', methods=['POST'])
def add_spells_to_book(owner, name):
    book = Book.query.filter_by(owner=owner, name=name).first()
    book.spells.append(Spell.query.filter_by(name=request.json.get("name")).first())

    db.session.add(book)
    db.session.commit()
    return "OK"


spellsinbook = db.Table('spellsinbook',
                        db.Column('spell', db.String(100),
                                  db.ForeignKey('spell.name'),
                                  primary_key=True),
                        db.Column('book', db.Integer,
                                  db.ForeignKey('book.id'),
                                  primary_key=True)
                        )


@dataclass
class Book(db.Model):
    name: str
    owner: str
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    owner = db.Column(db.String(80), nullable=False)
    spells = db.relationship('Spell', secondary=spellsinbook, lazy='subquery')


@dataclass
class Character(db.Model):
    name: str
    name = db.Column(db.String(80), primary_key=True)


@dataclass
class Spell(db.Model):
    name: str
    level: int
    school: str
    cast_time: str
    duration: str
    damage_type: str
    concentration: bool
    ritual: bool
    upcast: bool
    materials_consumed: bool
    materials_cost: float
    name = db.Column(db.String(100), primary_key=True)
    level = db.Column(db.Integer, default=1)
    school = db.Column(db.String(16), nullable=False)
    cast_time = db.Column(db.String(32), nullable=False)
    duration = db.Column(db.String(32))
    damage_type = db.Column(db.String(64))
    concentration = db.Column(db.Boolean, nullable=False, default=False)
    ritual = db.Column(db.Boolean, nullable=False, default=False)
    upcast = db.Column(db.Boolean, nullable=False, default=False)
    materials_consumed = db.Column(db.Boolean, nullable=False, default=False)
    materials_cost = db.Column(db.Float)
